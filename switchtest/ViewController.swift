//
//  ViewController.swift
//  switchtest
//
//  Created by Lena Brusilovski on 14/04/2019.
//  Copyright © 2019 Lena Brusilovski. All rights reserved.
//

import Lottie
import UIKit

class ViewController: UIViewController {
    var _switch: AnimatedSwitch!
    var _switch1: AnimatedSwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let animation = Animation.named("switch_states")
        _switch = AnimatedSwitch(animation: animation!)
        view.addSubview(_switch)
        _switch.center = view.center

        _switch.frame = CGRect(origin: view.center, size: CGSize(width: 71, height: 31))
        _switch.center = CGPoint(x: 100, y: view.center.y)

        let label = UILabel()
        label.text = "Good switch\nanimation"
        label.textColor = .black
        label.numberOfLines = 0
        view.addSubview(label)
        label.center = CGPoint(x: 50, y: view.center.y - 120)

        let animation1 = Animation.named("switch_animation1")
        _switch1 = AnimatedSwitch(animation: animation1!)
        _switch1.frame = CGRect(origin: view.center, size: CGSize(width: 71, height: 31))
        view.addSubview(_switch1)
        _switch1.center = CGPoint(x: 300, y: view.center.y)

        let label1 = UILabel()
        label1.text = "Bad switch\nanimation"
        label1.textColor = .black
        label1.numberOfLines = 0
        view.addSubview(label1)
        label1.sizeToFit()
        label.sizeToFit()
        label1.center = CGPoint(x: 250, y: view.center.y - 100)
    }
}
