//
//  CustomSwitch.swift
//  CustomSwitch
//
//  Created by Ivan Kovacevic on 15/12/2016.
//  Copyright © 2016 Ivan Kovacevic. All rights reserved.
//

import UIKit
import Lottie

final public class AnimatedSwitch1: AnimatedControl {
    
    let animationView: AnimationView
    /// The current state of the switch.
    public var isOn: Bool {
        set {
            /// This is forwarded to a private variable because the animation needs to be updated without animation when set externally and with animation when set internally.
            guard _isOn != newValue else { return }
            updateOnState(isOn: newValue, animated: false, shouldFireHaptics: false)
        }
        get {
            return _isOn
        }
    }
    
    /// Set the state of the switch and specify animation and haptics
    public func setIsOn(_ isOn: Bool, animated: Bool, shouldFireHaptics: Bool = true) {
        guard isOn != _isOn else { return }
        updateOnState(isOn: isOn, animated: animated, shouldFireHaptics: shouldFireHaptics)
    }
    
    /// Sets the play range for the given state. When the switch is toggled, the animation range is played.
    public func setProgressForState(fromProgress: AnimationProgressTime,
                                    toProgress: AnimationProgressTime,
                                    forOnState: Bool) {
        if forOnState {
            onStartProgress = fromProgress
            onEndProgress = toProgress
        } else {
            offStartProgress = fromProgress
            offEndProgress = toProgress
        }
        
        updateOnState(isOn: _isOn, animated: false, shouldFireHaptics: false)
    }
    
    public override init(animation: Animation) {
        /// Generate a haptic generator if available.
        #if os(iOS)
        if #available(iOS 10.0, *) {
            self.hapticGenerator = HapticGenerator()
        } else {
            self.hapticGenerator = NullHapticGenerator()
        }
        #else
        self.hapticGenerator = NullHapticGenerator()
        #endif
        self.animationView = AnimationView(animation: animation)
        super.init(animation: animation)
        updateOnState(isOn: _isOn, animated: false, shouldFireHaptics: false)
        self.accessibilityTraits = UIAccessibilityTraits.button
    }
    
    public override init() {
        /// Generate a haptic generator if available.
        #if os(iOS)
        if #available(iOS 10.0, *) {
            self.hapticGenerator = HapticGenerator()
        } else {
            self.hapticGenerator = NullHapticGenerator()
        }
        #else
        self.hapticGenerator = NullHapticGenerator()
        #endif
        self.animationView = AnimationView()
        super.init()
        updateOnState(isOn: _isOn, animated: false, shouldFireHaptics: false)
        self.accessibilityTraits = UIAccessibilityTraits.button
    }
    
    required public init?(coder aDecoder: NSCoder) {
        /// Generate a haptic generator if available.
        #if os(iOS)
        if #available(iOS 10.0, *) {
            self.hapticGenerator = HapticGenerator()
        } else {
            self.hapticGenerator = NullHapticGenerator()
        }
        #else
        self.hapticGenerator = NullHapticGenerator()
        #endif
        self.animationView = AnimationView()
        super.init(coder: aDecoder)
        self.accessibilityTraits = UIAccessibilityTraits.button
    }
    
    fileprivate var onStartProgress: CGFloat = 0
    fileprivate var onEndProgress: CGFloat = 1
    fileprivate var offStartProgress: CGFloat = 1
    fileprivate var offEndProgress: CGFloat = 0
    fileprivate var _isOn: Bool = false
    fileprivate var hapticGenerator: ImpactGenerator
    
    // MARK: Animation State
    
    func updateOnState(isOn: Bool, animated: Bool, shouldFireHaptics: Bool) {
        _isOn = isOn
        var startProgress = isOn ? onStartProgress : offStartProgress
        var endProgress = isOn ? onEndProgress : offEndProgress
        let finalProgress = endProgress
        
        let realtimeProgress = animationView.realtimeAnimationProgress
        
        let previousStateStart = isOn ? offStartProgress : onStartProgress
        let previousStateEnd = isOn ? offEndProgress : onEndProgress
        if realtimeProgress.isInRange(min(previousStateStart, previousStateEnd),
                                      max(previousStateStart, previousStateEnd)) {
            /// Animation is currently in the previous time range. Reverse the previous play.
            startProgress = previousStateEnd
            endProgress = previousStateStart
        }
        
        guard animated == true else {
            animationView.currentProgress = finalProgress
            return
        }
        
        if shouldFireHaptics {
            self.hapticGenerator.generateImpact()
        }
        
        animationView.play(fromProgress: startProgress, toProgress: endProgress, loopMode: LottieLoopMode.playOnce) { (finished) in
            if finished == true {
                self.animationView.currentProgress = finalProgress
            }
        }
        
        updateAccessibilityLabel()
    }
    
    public override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        updateOnState(isOn: !_isOn, animated: true, shouldFireHaptics: true)
        sendActions(for: .valueChanged)
    }
    
    public override func animationDidSet() {
        updateOnState(isOn: _isOn, animated: true, shouldFireHaptics: false)
    }
    
    // MARK: Private
    
    private func updateAccessibilityLabel() {
        accessibilityValue = _isOn ? NSLocalizedString("On", comment: "On") : NSLocalizedString("Off", comment: "Off")
    }
    
}

protocol ImpactGenerator {
    func generateImpact()
}

class NullHapticGenerator: ImpactGenerator {
    func generateImpact() {
        
    }
}

#if os(iOS)
@available(iOS 10.0, *)
class HapticGenerator: ImpactGenerator {
    func generateImpact() {
        impact.impactOccurred()
    }
    
    fileprivate let impact = UIImpactFeedbackGenerator(style: .light)
}
#endif

extension AnimationProgressTime {
    func isInRange(_ from: CGFloat, _ to: CGFloat) -> Bool {
        return (from < self && self < to)
    }
}
